import requests
import json
import configparser
import random

# 設定ファイル読み込み
config_ini = configparser.ConfigParser()
config_ini.read('config.ini', encoding='utf-8')

# 設定ファイルの値を変数に設定
user = config_ini['GENERAL']['User']
apibasepath = config_ini['GENERAL']['ApiBasePath']
queryparam = config_ini['GENERAL']['QueryParam']

# APIとして呼び出すURL
#  例：url = "http://api.booklog.jp/v2/json/shikihiiro?category_id=3128782&status=4&count=100"
url = apibasepath + user + queryparam

#requests.getを使うと、レスポンス内容を取得できるのでとりあえず変数へ保存
response = requests.get(url)

#response.json()でJSONデータに変換して変数へ保存
jsonData = response.json()

num = 0
for jsonObj in jsonData["books"]:
    if not jsonObj:
        # 配列が空の場合、終了
        break
    #print(jsonObj["title"])
    num += 1



print('===================')
# 抽出した書籍の数
print("Number of books:", num)
# ランダムなタイトルを出力
#  乱数生成（0〜num-1）
print(jsonData['books'][random.randint(0, num-1)]['title'])

